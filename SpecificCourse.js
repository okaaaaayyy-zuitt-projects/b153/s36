/* Activity:
	Populate the fields in our specific course Card Component with the data that we receive from the server

	Points to remember:
		- Use the ID to get the course's specific data from the proper endpoint

		- The fetch request should happen on page load/component mount

		- Find a way to move that data from the scope of a .then statement into your return statement

		When done, push to Gitlab and paste the link to React - Routing and Conditional Rendering

*/
import { useState, useEffect, useContext } from 'react'
import { Link, useParams } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext'

export default function SpecificCourse(){
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	const { user } = useContext(UserContext);
	// useParams() contains any value we are trying to pass in the URL, stored as a key/value pair
	// e.g.
	// courseId: 6221e686ca22c673a6296bea
	const { courseId } = useParams();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	// console.log(courseId)
	return(
		<Card className="my-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{name}</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>{description}</Card.Text>
				<h6>Price: {price}</h6>
			</Card.Body>
			<Card.Footer className="d-grid gap-2">
				{user.id !== null ?
					<Button variant="primary" block="true">Enroll</Button>
					:
					<Link className="btn btn-danger btn-block" to="/login">Log In tto Enroll</Link>
				}
			</Card.Footer>
		</Card>
	)
}